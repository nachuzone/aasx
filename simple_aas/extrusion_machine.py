from flask import Flask, request, jsonify
from datetime import datetime
import json
import random

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "<h1>Main page</h1>"

@app.route('/maintain-done')
def hello_maintain():
    headers = request.headers
    auth = headers.get('X-Api-Key')
    if auth == 'abc123ABC!@#':
        return "<h2>Maintain Page</h2>", 200
    else:
        return jsonify({'message':'Not OK: Not Authorized'}), 401
#    return 

@app.route('/some-data')
def hello_some_data():
    temperature_value = random.randint(1, 23)
    pressure_values = random.randint(5,10)
    data = {"Temperature": temperature_value, "Pressure": pressure_values}
    return data

@app.route('/maintain-done', methods=['POST'])
def maintain_done():
    # Get the current time
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Get 'status' and 'comment' from the request headers
    status = request.headers.get('status', '')
    comment = request.headers.get('comment', '')

    # Write the current time, status, and comment to a text file
    with open('maintenance_log.txt', 'a') as f:
        f.write(f'Time: {current_time}, Status: {status}, Comment: {comment}\n')

    # Generate a JSON response with the current time
    response = jsonify({'current_time': current_time})
    return response

@app.route('/get-maintenance-log', methods=['GET'])
def get_maintenance_log():
    # Open the text file in read mode
    with open('maintenance_log.txt', 'r') as f:
        # Read the contents of the file
        log_data = f.read()

    # Return the log data as a response
    return log_data


# header testing
@app.route("/auth-head")
def auth_header_endpoint():
    headers = request.headers
    auth = headers.get('X-Api-Key')
    if auth == 'abc123ABC!@#':
        return jsonify({'message':'OK: Authorized'}), 200
    else:
        return jsonify({'message':'Not OK: Not Authorized'}), 401

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0',port=5001)
